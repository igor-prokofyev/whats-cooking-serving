import pandas as pd
import mlflow
import os
from dotenv import load_dotenv
from fastapi import FastAPI, File, UploadFile, HTTPException, Form
from prometheus_fastapi_instrumentator import Instrumentator
import joblib
import logging
from prometheus_client import Gauge
from fastapi.responses import HTMLResponse

# Load the environment variables from the .env file into the application
load_dotenv()

# Initialize the FastAPI application
app = FastAPI()

# Initialize Prometheus endpoint
Instrumentator().instrument(app).expose(app)

# Create a class to store the deployed model & use it for prediction
class Model:
    def __init__(self, model_name, model_version):
        """
        Initialize the model
        model_name: Name of the model in registry
        model_version: Version of the model
        """
        try:
            # Load the model from the registry by version
            self.model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_version}")
            logging.info(f"Model {model_name} version {model_version} loaded successfully.")
            
            # Get run_id associated with the model version
            client = mlflow.tracking.MlflowClient()
            model_version_details = client.get_model_version(model_name, str(model_version))
            self.run_id = model_version_details.run_id

            # Load the saved TF-IDF vectorizer
            tfidf_path = mlflow.artifacts.download_artifacts(run_id=self.run_id, artifact_path="tfidf_vectorizer.pkl")
            self.tfidf_vectorizer = joblib.load(tfidf_path)
            logging.info("TF-IDF vectorizer loaded successfully.")
        except mlflow.exceptions.RestException as e:
            logging.error(f"Failed to load model {model_name} version {model_version}: {e}")
            raise

    def predict(self, data):
        """
        Use the loaded model to make predictions on the data
        data: Pandas DataFrame to perform predictions
        """
        # Transform data using the TF-IDF vectorizer
        data_tfidf = self.tfidf_vectorizer.transform(data['ingredients'])
        transformed_data = pd.DataFrame(data_tfidf.toarray())
        predictions = self.model.predict(transformed_data)
        return predictions.tolist()  # Convert numpy array to list for JSON serialization

# Try to create model and handle potential errors
try:
    model = Model("cuisine_svc_model", 12)
except mlflow.exceptions.RestException as e:
    model = None
    logging.error("Model could not be loaded. Exiting.")
    exit(1)

@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    if model is None:
        raise HTTPException(status_code=500, detail="Model is not available.")
    
    if file.filename.endswith(".csv"):
        # Create a temporary file with the same name as the uploaded CSV file to load the data into a pandas DataFrame
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        
        # Ensure the DataFrame has the expected format
        if 'ingredients' not in data.columns:
            raise HTTPException(status_code=400, detail="Invalid file format. The CSV file must contain an 'ingredients' column.")
        
        # Return an object containing the model predictions
        return list(model.predict(data))
    else:
        raise HTTPException(status_code=400, detail="Invalid file format. Only CSV Files accepted.")

@app.get("/", response_class=HTMLResponse)
async def read_root():
    with open("/code/app/prediction_form.html", "r") as f:
        html_content = f.read()
    return HTMLResponse(content=html_content)

@app.post("/predict")
async def predict_from_string(ingredients: dict):
    if model is None:
        raise HTTPException(status_code=500, detail="Model is not available.")
    
    # Prepare the data
    data = pd.DataFrame({"ingredients": [ingredients["ingredients"]]})
    
    # Return the prediction
    predictions = model.predict(data)
    return {"predictions": predictions}

# Check if the environment variables for AWS access are available.
if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    logging.error("AWS credentials are not available. Exiting.")
    exit(1)
