FROM python:3.10

WORKDIR /code

# Install PDM and create virtual environment
RUN python --version \
    && curl -sSL https://pdm-project.org/install-pdm.py | python3 - \
    && export PATH="/root/.local/bin:$PATH" \
    && /root/.local/bin/pdm venv create --force

# Add PDM to the PATH
ENV PATH="/root/.local/bin:$PATH"

# Copy dependency files and install dependencies
COPY ./pyproject.toml /code/
COPY ./pdm.lock /code/
RUN pdm install

# Copy application code
COPY ./inference.py /code/app/inference.py
COPY ./prediction_form.html /code/app/prediction_form.html
COPY ./.env /code/app/.env

CMD ["pdm", "run", "uvicorn", "app.inference:app", "--host", "0.0.0.0", "--port", "80"]
